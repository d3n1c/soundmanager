#!/usr/bin/php

<?php

require '../../vendor/autoload.php';
require '../config.php';

if (empty($argv[1])) {
  exit;
}

$data = base64_decode($argv[1]);
$data = json_decode($data, TRUE);
$data = empty($data) ? [] : (!is_array($data) ? [$data] : $data);

$stop = empty($argv[2]) ? FALSE : ($argv[2] != 1 ? FALSE : TRUE);
if (!empty($stop)) {
  $cmd = __DIR__ . '/playstop.php';
  shell_exec($cmd);
  unset ($cmd);
}
unset ($stop);

file_put_contents(Flight::get('dncconf')['playlistPath'], json_encode($data), LOCK_EX);
