#!/usr/bin/php

<?php

require '../../vendor/autoload.php';
require '../config.php';

$PLAYBIN = '/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'/usr/bin/mplayer ';
$storage = Flight::get('dncconf')['queuePath'];
$filename = 'playit';

$i = 0;
while ($i < 1) {
  $path = $storage . '/' . $filename;
  clearstatcache();
  if (is_file($path)) {
    $audio = file_get_contents($path);
    unlink($path);
    if (!empty($audio)) {
      $playit = FALSE;
      if (!preg_match('/^http/i', $audio)) {
        clearstatcache();
        if (is_file($audio)) {
          $playit = TRUE;
        }
      }
      else {
        $playit = TRUE;
      }
      
      if (!empty($playit)) {
        file_put_contents($storage . '/hold', $audio, LOCK_EX);
        shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 10%\'');
        shell_exec($PLAYBIN . ' ' . $audio . '\'');
      }
      clearstatcache();
      if (is_file($storage . '/hold')) {
        unlink($storage . '/hold');
      }
    }
  }
  else {
    // check pause
    clearstatcache();
    if (is_file(Flight::get('dncconf')['pausePath'])) {
      clearstatcache();
      if (is_file($storage . '/pauserelease')) {
        unlink(Flight::get('dncconf')['pausePath']);
        unlink($storage . '/pauserelease');
      }
    }
  }
  unset ($path);
  
  clearstatcache();
  if (is_file(Flight::get('dncconf')['playlistPath'])) {
    $list = file_get_contents(Flight::get('dncconf')['playlistPath']);
    $list = empty($list) ? [] : json_decode($list, TRUE);
    if (!empty($list[0])) {
      file_put_contents($storage . '/' . $filename, $list[0]);
      unset($list[0]);
      $dump = [];
      if (!empty($list)) {
        foreach ($list as $values) {
          $dump[] = $values;
        }
      }
      $list = $dump;
      unset ($dump);
    }
    file_put_contents(Flight::get('dncconf')['playlistPath'], json_encode($list), LOCK_EX);
    unset ($list);
  }
  sleep(3);
}
