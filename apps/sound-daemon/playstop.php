#!/usr/bin/php

<?php

require '../../vendor/autoload.php';
require '../config.php';

for ($i = 0; $i < 4; $i++) {
  shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'/usr/bin/amixer set Master 10%-\'');
  sleep(2);
}

clearstatcache();
if (is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
  shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'echo "stop" > ~/.mplayer/fifo\'');
  unlink(Flight::get('dncconf')['queuePath'] . '/hold');
}

sleep(1);
shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'/usr/bin/amixer set Master 10%\'');
