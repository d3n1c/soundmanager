#!/usr/bin/php

<?php

require '../../vendor/autoload.php';
require '../config.php';

date_default_timezone_set(Flight::get('dncconf')['defaultTZ']);

$storagePath = Flight::get('dncconf')['schedulePath'];
$pausePath = Flight::get('dncconf')['pausePath'];

clearstatcache();
if (!is_dir($storagePath)) {
  mkdir($storagePath, 0755);
}

function listing () {
  global $storagePath;
  $result = scandir($storagePath);
  $return = [];
  foreach ($result as $values) {
    if ($values != '.' && $values != '..') {
      $return[] = $values;
    }
  }
  unset ($result);
  sort($return);
  return $return;
}

$playnow = NULL;
$cnt = 0;
while ($cnt < 1) {
  sleep(3);
  
  // check pause
  clearstatcache();
  if (is_file($pausePath)) {
    $pause = file_get_contents($pausePath);
    if (!empty($pause)) {
      unset ($pause);
      continue;
    }
    unset ($pause);
  }

  $filename = date('Y-m-d_H-i', time());
  clearstatcache();
  if (is_file($storagePath . '/' . $filename)) {
    if ($filename != $playnow) {
      $legacy = $storagePath . '/' . $filename;
      $forced = TRUE;
      $playnow = $filename;
    }
  }
  
  if (empty($legacy)) {
    // check whether player is busy or not
    clearstatcache();
    if (is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
      clearstatcache();
      if (!is_file(Flight::get('dncconf')['keepVol'])) {
        // get volume state
        $vol = shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer sget Master\' | grep \'Left:\' | awk -F\'[][]\' \'{ print $2 }\'');
        $vol = trim($vol);
        settype($vol, 'int');
        if ($vol <= 40) {
          shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 10%\'');
          sleep(1);
          for ($i = 0; $i < 6; $i++) {
            shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 10%+\'');
            sleep(1);
          }
        }
      }
      else {
        $keepVol = file_get_contents(Flight::get('dncconf')['keepVol']);
        shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master ' . $keepVol . '\'');
      }
      continue;
    }
    
    // check whether existing base list or not
    clearstatcache();
    if (is_file(Flight::get('dncconf')['playlistPath'])) {
      $baselist = file_get_contents(Flight::get('dncconf')['playlistPath']);
      $baselist = empty($baselist) ? [] : json_decode($baselist, TRUE);
      if (!empty($baselist)) {
        unset ($baselist);
        continue;
      }
      unset ($baselist);
    }

    $list = listing();
    if (!empty($list)) {
      $old = NULL;
      sort($list);
      foreach ($list as $values) {
        if ($values > $filename) {
          break;
        }
        $legacy = $storagePath . '/' . $values;
        $playnow = $values;
        if ($values != $old) {
          if (!empty($old)) {
            clearstatcache();
            if (is_file($storagePath . '/' . $old)) {
              unlink($storagePath . '/' . $old);
            }
          }
          $old = $values;
        }
      }
      unset ($old);
    }
    unset ($list);
  }
  
  if (!empty($legacy)) {
    $data = file_get_contents($legacy);
    if (!empty($data)) {
      sleep(3);
      $cmd = 'cd ' . __DIR__;
      $cmd .= '; ./changelist.php ' . base64_encode($data) . (!empty($forced) ? ' 1' : NULL);
      shell_exec($cmd);
      unset ($cmd);
      
      $stop = 0;
      $count = 0;
      while($stop < 1) {
        clearstatcache();
        if (!is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
          $count++;
          if ($count > 20) {
            $stop = 1;
          }
        }
        else {
          clearstatcache();
          if (is_file(Flight::get('dncconf')['keepVol'])) {
            $keepVol = file_get_contents(Flight::get('dncconf')['keepVol']);
            shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master ' . $keepVol . '\'');
          }
          else {
            shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 10%\'');
            for ($i = 0; $i < 5; $i++) {
              shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 10%+\'');
              sleep(1);
            }
          }
          $stop = 1;
        }
        sleep(1);
      }
      unset ($count, $stop);
    }
    unset ($data);
  }
  unset ($forced, $legacy, $filename);
}
