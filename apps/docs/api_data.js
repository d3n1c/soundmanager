define({ "api": [
  {
    "type": "post",
    "url": "/playnow",
    "title": "Push to play now",
    "name": "PlayNow",
    "group": "Play_Now",
    "version": "0.1.0",
    "description": "<p>Push the player to play requested sounds</p>",
    "parameter": {
      "fields": {
        "Query string": [
          {
            "group": "Query string",
            "type": "String",
            "optional": true,
            "field": "pausepost",
            "description": "<p>Optional set pause holder after play action.</p>"
          },
          {
            "group": "Query string",
            "type": "String",
            "optional": true,
            "field": "repeatedly",
            "description": "<p>Optional keep the list after play action.</p>"
          }
        ],
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Required request data in json formatted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Query pausepost string example",
          "content": "/?pausepost=1",
          "type": "string"
        },
        {
          "title": "Query repeatedly string example",
          "content": "/?repatedly=1",
          "type": "string"
        },
        {
          "title": "Body data example",
          "content": "[\n   \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3\",\n   \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Al_Baqarah.mp3\",\n   \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_As_Safat.mp3\"\n]",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>of the action</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example success output",
          "content": "{ \"result\": \"done\" }",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Play_Now"
  },
  {
    "type": "get",
    "url": "/state/:state",
    "title": "Send state action to the player",
    "name": "SendPlayerState",
    "group": "Player_Action",
    "version": "0.1.0",
    "description": "<p>Send action state to the player The status of the action is symbolized by a numeric number with the following reference 0 =&gt; stop, 1 =&gt; pause, 2 =&gt; play</p>",
    "parameter": {
      "examples": [
        {
          "title": "Parameter player STOP example",
          "content": "/state/0",
          "type": "string"
        },
        {
          "title": "Parameter player PAUSE example",
          "content": "/state/1",
          "type": "string"
        },
        {
          "title": "Parameter player PLAY example",
          "content": "/state/2",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>action done</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Confirm success output",
          "content": "{\n    \"result\": \"done\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Player_Action"
  },
  {
    "type": "get",
    "url": "/list",
    "title": "Get Listing of playing",
    "name": "GetPlayList",
    "group": "Player_Listing",
    "version": "0.1.0",
    "description": "<p>Get listing of the playing</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>state of the player without query parameters</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve success output",
          "content": "{\n    \"result\": [\n        {\n            \"id\": \"2019-03-09_11-40\", \n            \"name\": \"Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3\"\n        }, \n        {\n            \"id\": \"2019-03-10_10-00\", \n            \"name\": \"Zain_A_-_Surah_Al-Waaqiah.mp3\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "Object",
            "optional": false,
            "field": "Not",
            "description": "<p>Found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve on error",
          "content": "{\n   \"error\": \"not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Player_Listing"
  },
  {
    "type": "post",
    "url": "/list",
    "title": "Updating listing of playing",
    "name": "UpdatePlayList",
    "group": "Player_Listing",
    "version": "0.1.0",
    "description": "<p>Updating list of the playing</p>",
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "Body",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Required request data in json formatted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Body data example",
          "content": "[\n  { \"time\": \"2019-03-10 15:12:34\", \"delete\": 1 },\n  {\n    \"time\": \"2019-03-10 17:02:21\", \"list\":\n    [\n      \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3\",\n      \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Al_Baqarah.mp3\",\n      \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_As_Safat.mp3\"\n    ]\n  }\n]",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>of the action</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example success output",
          "content": "{ \"result\": true }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "Object",
            "optional": false,
            "field": "Failed",
            "description": "<p>Data not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example on error",
          "content": "{\n   \"error\": \"data not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Player_Listing"
  },
  {
    "type": "get",
    "url": "/",
    "title": "Get Player State",
    "name": "GetPlayerState",
    "group": "Player_State",
    "version": "0.1.0",
    "description": "<p>Get state of the player</p>",
    "parameter": {
      "fields": {
        "Query string": [
          {
            "group": "Query string",
            "type": "String",
            "optional": true,
            "field": "getstate",
            "description": "<p>Optional just check the player state.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Query string example",
          "content": "/?getstate=1",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>state of the player with query parameters</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "With query parameters",
          "content": "{\n  \"result\": 1\n}",
          "type": "json"
        },
        {
          "title": "Without query parameters",
          "content": "{\n  \"result\": \"Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "Object",
            "optional": false,
            "field": "Not",
            "description": "<p>Found</p>"
          },
          {
            "group": "Error",
            "type": "Object",
            "optional": false,
            "field": "with",
            "description": "<p>query parameters is no output result</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "without query parameters",
          "content": "{\n   \"error\": \"not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Player_State"
  },
  {
    "type": "get",
    "url": "/volume/:state",
    "title": "Send volume action to the player",
    "name": "SendPlayerVolume",
    "group": "Player_Volume",
    "version": "0.1.0",
    "description": "<p>Send action to manipulate volume of the player The state of the action is symbolized by a string with the following reference &quot;up&quot; =&gt; volume up (+), &quot;down&quot; =&gt; volume down (-)</p>",
    "parameter": {
      "examples": [
        {
          "title": "Parameter volume \"UP\" example",
          "content": "/volume/up",
          "type": "string"
        },
        {
          "title": "Parameter volume \"Down\" example",
          "content": "/volume/down",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>action done</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Confirm success output",
          "content": "{\n    \"result\": \"done\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Player_Volume"
  },
  {
    "type": "get",
    "url": "/list/:id",
    "title": "Get Detail of Playlist",
    "name": "PlayListDetail",
    "group": "Playlist_Detail",
    "version": "0.1.0",
    "description": "<p>Get detail of the playlist</p>",
    "parameter": {
      "examples": [
        {
          "title": "Parameter ID example",
          "content": "/list/2019-03-10_17-44",
          "type": "string"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>listing of sounds will be play in the list</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve success output",
          "content": "{\n    \"result\": [\n        \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3\", \n        \"/home/usman/sounds/Zain_A_-_Surah_Al-Waaqiah.mp3\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "Object",
            "optional": false,
            "field": "Not",
            "description": "<p>Found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve on error",
          "content": "{\n   \"error\": \"not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Playlist_Detail"
  },
  {
    "type": "get",
    "url": "/volrelease",
    "title": "Release holded volume",
    "name": "ReleaseVolume",
    "group": "Release_Volume",
    "version": "0.1.0",
    "description": "<p>Send action to the player to release holded volume. When user send action state to manipulate player's volume, system will hold the state to keep the volume step, prevent daemon to change the state. So... this request will releasing the holder then daemon can take care the volume itself</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>action done</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Confirm success output",
          "content": "{\n    \"result\": \"done\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Release_Volume"
  },
  {
    "type": "get",
    "url": "/sounds",
    "title": "Get Listing of available sounds",
    "name": "GetSounds",
    "group": "Sounds_Listing",
    "version": "0.1.0",
    "description": "<p>Get listing of available sounds in the server</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>available sounds in the server's storage</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve success output",
          "content": "{\n    \"result\": [\n        {\n            \"name\": \"Maher_Al_Muaiqly_-_Surah_Maryam.mp3\", \n            \"path\": \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3\"\n        }, \n        {\n            \"name\": \"Zain_A_-_Surah_Al-Waaqiah.mp3\", \n            \"path\": \"/home/usman/sounds/Zain_A_-_Surah_Al-Waaqiah.mp3\"\n        },\n        {\n            \"name\": \"Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3\",\n            \"path\": \"/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error": [
          {
            "group": "Error",
            "type": "Object",
            "optional": false,
            "field": "Not",
            "description": "<p>Found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve on error",
          "content": "{\n   \"error\": \"not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../playlist/route.php",
    "groupTitle": "Sounds_Listing"
  }
] });
