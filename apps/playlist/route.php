<?php

require __DIR__ . '/../config.php';
require __DIR__ . '/functions.php';

/**
 * @api {get} / Get Player State
 * @apiName GetPlayerState
 * @apiGroup Player State
 * @apiVersion 0.1.0
 * @apiDescription Get state of the player
 * 
 * @apiParam (Query string) {String} [getstate] Optional just check the player state.
 * @apiParamExample {string} Query string example
 * /?getstate=1
 * 
 * @apiSuccess {Object} result state of the player with query parameters
 * @apiSuccessExample With query parameters
 * {
 *   "result": 1
 * }
 * 
 * @apiSuccess {Object} result state of the player without query parameters
 * @apiSuccessExample Without query parameters
 * {
 *   "result": "Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3"
 * }
 * 
 * @apiError (Error) {Object} Not Found
 * @apiErrorExample without query parameters
 * {
 *    "error": "not found"
 * }
 * 
 * @apiError (Error) {Object} with query parameters is no output result
 * 
 */
Flight::route('OPTIONS|GET /', function() {
  if (Flight::request()->method == 'GET') {
    if (!empty(Flight::request()->query->getstate)) {
      clearstatcache();
      if (!is_file(Flight::get('dncconf')['pausePath'])) {
        clearstatcache();
        if (is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
          Flight::json(['result' => 1]);
        }
      }
      return;
    }
    $result = Flight::whatisplayingnow();
    $result = empty($result) ? ['error' => 'not found'] : ['result' => $result];
    Flight::json($result);
    unset ($result);
    return;
  }
});

/**
 * @api {get} /list Get Listing of playing
 * @apiName GetPlayList
 * @apiGroup Player Listing
 * @apiVersion 0.1.0
 * @apiDescription Get listing of the playing
 * 
 * @apiSuccess {Object} result state of the player without query parameters
 * @apiSuccessExample Retrieve success output
 * {
 *     "result": [
 *         {
 *             "id": "2019-03-09_11-40", 
 *             "name": "Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3"
 *         }, 
 *         {
 *             "id": "2019-03-10_10-00", 
 *             "name": "Zain_A_-_Surah_Al-Waaqiah.mp3"
 *         }
 *     ]
 * }
 * 
 * @apiError (Error) {Object} Not Found
 * @apiErrorExample Retrieve on error
 * {
 *    "error": "not found"
 * }
 */

/** 
 * @api {post} /list Updating listing of playing
 * @apiName UpdatePlayList
 * @apiGroup Player Listing
 * @apiVersion 0.1.0
 * @apiDescription Updating list of the playing
 * 
 * @apiParam (Body) {String} data Required request data in json formatted.
 * @apiParamExample {json} Body data example
 * [
 *   { "time": "2019-03-10 15:12:34", "delete": 1 },
 *   {
 *     "time": "2019-03-10 17:02:21", "list":
 *     [
 *       "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3",
 *       "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Al_Baqarah.mp3",
 *       "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_As_Safat.mp3"
 *     ]
 *   }
 * ]
 * 
 * @apiSuccess {Object} result of the action
 * @apiSuccessExample Example success output
 * { "result": true }
 * 
 * @apiError (Error) {Object} Failed Data not found
 * @apiErrorExample Example on error
 * {
 *    "error": "data not found"
 * }
 * 
 */
Flight::route('OPTIONS|GET|POST /list', function() {
  if (Flight::request()->method == 'GET') {
    $result = Flight::getschedule(TRUE);
    $result = empty($result) ? ['error' => 'not found'] : ['result' => $result];
    Flight::json($result);
    unset ($result);
    return;
  }
  
  if (Flight::request()->method == 'POST') {
    $data = Flight::request()->getBody();
    $data = json_decode($data, TRUE);
    $result = Flight::updateschedule($data);
    unset ($data);
    Flight::json($result);
    unset ($result);
    return;
  }
});

/**
 * @api {get} /list/:id Get Detail of Playlist
 * @apiName PlayListDetail
 * @apiGroup Playlist Detail
 * @apiVersion 0.1.0
 * @apiDescription Get detail of the playlist
 * 
 * @apiParamExample {string} Parameter ID example
 * /list/2019-03-10_17-44
 * 
 * @apiSuccess {Object} result listing of sounds will be play in the list
 * @apiSuccessExample Retrieve success output
 * {
 *     "result": [
 *         "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3", 
 *         "/home/usman/sounds/Zain_A_-_Surah_Al-Waaqiah.mp3"
 *     ]
 * }
 * 
 * @apiError (Error) {Object} Not Found
 * @apiErrorExample Retrieve on error
 * {
 *    "error": "not found"
 * }
 */
Flight::route('OPTIONS|GET /list/@id', function($id) {
  $result = Flight::scheduledetail($id);
  $result = empty($result) ? ['error' => 'not found'] : ['result' => $result];
  Flight::json($result);
  unset ($result);
  return;
});

/**
 * @api {get} /state/:state Send state action to the player
 * @apiName SendPlayerState
 * @apiGroup Player Action
 * @apiVersion 0.1.0
 * @apiDescription Send action state to the player
 * The status of the action is symbolized by a numeric number with the following reference
 * 0 => stop, 1 => pause, 2 => play
 * 
 * @apiParamExample {string} Parameter player STOP example
 * /state/0
 * @apiParamExample {string} Parameter player PAUSE example
 * /state/1
 * @apiParamExample {string} Parameter player PLAY example
 * /state/2
 * 
 * @apiSuccess {Object} result action done
 * @apiSuccessExample Confirm success output
 * {
 *     "result": "done"
 * }
 */
Flight::route('OPTIONS|GET /state/@state:[0-2]{1}', function($state) {
  if (empty($state)) {
    Flight::playerstop();
  }
  elseif($state == 1) {
    Flight::playerpause();
  }
  elseif($state == 2) {
    Flight::playerplay();
  }
  Flight::json(['result' => 'done']);
  return;
});

/**
 * @api {get} /volume/:state Send volume action to the player
 * @apiName SendPlayerVolume
 * @apiGroup Player Volume
 * @apiVersion 0.1.0
 * @apiDescription Send action to manipulate volume of the player
 * The state of the action is symbolized by a string with the following reference
 * "up" => volume up (+), "down" => volume down (-)
 * 
 * @apiParamExample {string} Parameter volume "UP" example
 * /volume/up
 * @apiParamExample {string} Parameter volume "Down" example
 * /volume/down
 * 
 * @apiSuccess {Object} result action done
 * @apiSuccessExample Confirm success output
 * {
 *     "result": "done"
 * }
 */
Flight::route('OPTIONS|GET /volume/@state', function($state) {
  file_put_contents(Flight::get('dncconf')['keepVol'], 1);
  if ($state == 'up') {
    Flight::increase();
  }
  else {
    Flight::decrease();
  }
  Flight::json(['result' => 'done']);
  return;
});

/**
 * @api {get} /volrelease Release holded volume
 * @apiName ReleaseVolume
 * @apiGroup Release Volume
 * @apiVersion 0.1.0
 * @apiDescription Send action to the player to release holded volume.
 * When user send action state to manipulate player's volume,
 * system will hold the state to keep the volume step,
 * prevent daemon to change the state. So... this request
 * will releasing the holder then daemon can take care the volume itself
 * 
 * @apiSuccess {Object} result action done
 * @apiSuccessExample Confirm success output
 * {
 *     "result": "done"
 * }
 */
Flight::route('OPTIONS|GET /volrelease', function() {
  clearstatcache();
  if (is_file(Flight::get('dncconf')['keepVol'])) {
    unlink(Flight::get('dncconf')['keepVol']);
  }
  Flight::json(['result' => 'done']);
  return;
});

/** 
 * @api {post} /playnow Push to play now
 * @apiName PlayNow
 * @apiGroup Play Now
 * @apiVersion 0.1.0
 * @apiDescription Push the player to play requested sounds
 * 
 * @apiParam (Query string) {String} [pausepost] Optional set pause holder after play action.
 * @apiParamExample {string} Query pausepost string example
 * /?pausepost=1
 * 
 * @apiParam (Query string) {String} [repeatedly] Optional keep the list after play action.
 * @apiParamExample {string} Query repeatedly string example
 * /?repatedly=1
 * 
 * @apiParam (Body) {String} data Required request data in json formatted.
 * @apiParamExample {json} Body data example
 * [
 *    "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3",
 *    "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Al_Baqarah.mp3",
 *    "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_As_Safat.mp3"
 * ]
 * 
 * @apiSuccess {Object} result of the action
 * @apiSuccessExample Example success output
 * { "result": "done" }
 * 
 */
Flight::route('OPTIONS|POST /playnow', function() {
  $pausepost = !empty(Flight::request()->query->pausepost) ? TRUE : FALSE;
  $repeatedly = !empty(Flight::request()->query->repeatedly) ? TRUE : FALSE;
  if (Flight::request()->method == 'POST') {
    $data = Flight::request()->getBody();
    $data = json_decode($data, TRUE);
    Flight::forcedplay($data, $pausepost, $repeatedly);
    unset ($data);
  }
  Flight::json(['result' => 'done']);
  return;
});

/**
 * @api {get} /sounds Get Listing of available sounds
 * @apiName GetSounds
 * @apiGroup Sounds Listing
 * @apiVersion 0.1.0
 * @apiDescription Get listing of available sounds in the server
 * 
 * @apiSuccess {Object} result available sounds in the server's storage
 * @apiSuccessExample Retrieve success output
 * {
 *     "result": [
 *         {
 *             "name": "Maher_Al_Muaiqly_-_Surah_Maryam.mp3", 
 *             "path": "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Maryam.mp3"
 *         }, 
 *         {
 *             "name": "Zain_A_-_Surah_Al-Waaqiah.mp3", 
 *             "path": "/home/usman/sounds/Zain_A_-_Surah_Al-Waaqiah.mp3"
 *         },
 *         {
 *             "name": "Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3",
 *             "path": "/home/usman/sounds/Maher_Al_Muaiqly_-_Surah_Ar_Rahman.mp3"
 *         }
 *     ]
 * }
 * 
 * @apiError (Error) {Object} Not Found
 * @apiErrorExample Retrieve on error
 * {
 *    "error": "not found"
 * }
 */
Flight::route('OPTIONS|GET /sounds', function() {
  if (Flight::request()->method == 'GET') {
    $result = Flight::searchdata();
    $result = empty($result) ? ['error' => 'not found'] : ['result' => $result];
    Flight::json($result);
    unset ($result);
  }
});