<?php

Flight::map('whatisplayingnow', function() {
  clearstatcache();
  if (!is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
    return;
  }
  $result = file_get_contents(Flight::get('dncconf')['queuePath'] . '/hold');
  return empty($result) ? NULL : basename($result);
});

Flight::map('scheduledetail', function($id) {
  $path = Flight::get('dncconf')['schedulePath'] . '/' . $id;
  clearstatcache();
  if (!is_file($path)) {
    unset ($path);
    return;
  }
  $result = file_get_contents($path);
  unset ($path);
  return json_decode($result);
});

Flight::map('getschedule', function($withname = FALSE) {
  $path = Flight::get('dncconf')['schedulePath'];
  clearstatcache();
  if (!is_dir($path)) {
    unset ($path);
    return;
  }
  
  $result = [];
  $handle = scandir($path);
  unset ($path);
  foreach ($handle as $values) {
    if ($values != '.' && $values != '..') {
      if (!empty($withname)) {
        $detail = Flight::scheduledetail($values);
        if (!empty($detail)) {
          $values = [
            'id' => $values,
            'name' => basename($detail[0])
          ];
        }
        unset ($detail);
      }
      $result[] = $values;
    }
  }
  unset ($handle);
  return $result;
});

Flight::map('updateschedule', function(array $data = []) {
  if (empty($data)) {
    unset ($data);
    return ['error' => 'No data found'];
  }
  
  $path = Flight::get('dncconf')['schedulePath'];
  clearstatcache();
  if (!is_dir($path)) {
    mkdir($path, 0755);
  }
  
  foreach ($data as $values) {
    if (empty($values['time'])) {
      continue;
    }
    
    $fname = date('Y-m-d_H-i', strtotime($values['time']));

    if (!empty($values['list'])) {
      $filename = $path . '/' . $fname;
      file_put_contents($filename, json_encode($values['list']), LOCK_EX);
      unset ($filename);
    }
    elseif (!empty($values['delete'])) {
      clearstatcache();
      if (is_file($path . '/' . $fname)) {
        unlink($path . '/' . $fname);
      }
    }
    unset ($fname);
  }
  unset ($path);
  return ['result' => TRUE];
});

Flight::map('volstate', function() {
  $result = shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer sget Master\' | grep \'Left:\' | awk -F\'[][]\' \'{ print $2 }\'');
  $result = trim($result);
  settype($result, 'int');
  return $result;
});

Flight::map('increase', function() {
  $state = Flight::volstate();
  file_put_contents(Flight::get('dncconf')['keepVol'], ($state + 10) . '%');
  shell_exec('/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 10%+\'');
});

Flight::map('decrease', function() {
  $state = Flight::volstate();
  file_put_contents(Flight::get('dncconf')['keepVol'], ($state - 10) . '%');
  shell_exec('/usr/bin/ssh '. Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 10%-\'');
});

Flight::map('playerstop', function() {
  file_put_contents(Flight::get('dncconf')['pausePath'], 1, LOCK_EX);
  $cmd = 'cd ' . __DIR__ . '/../sound-daemon';
  $cmd .= '; ./changelist.php ' . base64_encode(json_encode([])) . ' 1';
  $cmd .= '; cd ' . __DIR__;
  shell_exec($cmd);
  unset ($cmd);
});

Flight::map('playerpause', function() {
  file_put_contents(Flight::get('dncconf')['pausePath'], 1, LOCK_EX);
  
  $list = [];
  clearstatcache();
  if (is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
    $list[] = file_get_contents(Flight::get('dncconf')['queuePath'] . '/hold');
  }
  
  clearstatcache();
  if (is_file(Flight::get('dncconf')['playlistPath'])) {
    $dump = file_get_contents(Flight::get('dncconf')['playlistPath']);
    $dump = empty($dump) ? [] : json_decode($dump, TRUE);
    if (!empty($dump)) {
      foreach ($dump as $values) {
        $list[] = $values;
      }
    }
    unset ($dump);
  }
  
  if (!empty($list)) {
    file_put_contents(Flight::get('dncconf')['reservePath'], json_encode($list), LOCK_EX);
  }
  Flight::playerstop();
});

Flight::map('playerplay', function() {
  clearstatcache();
  if (is_file(Flight::get('dncconf')['reservePath'])) {
    $sndlist = file_get_contents(Flight::get('dncconf')['reservePath']);
    $sndlist = empty($sndlist) ? json_encode([]) : $sndlist;
    $cmd = 'cd ' . __DIR__ . '/../sound-daemon';
    $cmd .= '; ./changelist.php ' . base64_encode($sndlist);
    $cmd .= '; cd ' . __DIR__;
    unset ($sndlist);
    shell_exec($cmd);
    unset ($cmd);
    unlink(Flight::get('dncconf')['reservePath']);
  }
  
  clearstatcache();
  if (is_file(Flight::get('dncconf')['pausePath'])) {
    unlink(Flight::get('dncconf')['pausePath']);
  }

  clearstatcache();
  if (!is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
    $cmd = '/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 20%\'';
    shell_exec($cmd);
    unset ($cmd);
  }
  
  $stop = NULL;
  $state = 0;
  while (empty($stop)) {
    clearstatcache();
    if (is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
      for ($i = 0; $i < 4; $i++) {
        Flight::increase();
        sleep(2);
      }
      $stop = TRUE;
    }
    else {
      sleep(1);
      $state ++;
      if ($state > 20) {
        $stop = TRUE;
      }
    }
  }
  unset ($stop, $state);
});

Flight::map('forcedplay', function($data, $pause = FALSE, $repeat = FALSE) {
  if (empty($data) || count($data) < 1) {
    return;
  }
  
  Flight::playerstop();
  
  $playdata = json_encode($data);
  $cmd = 'cd ' . __DIR__ . '/../sound-daemon';
  $cmd .= '; ./changelist.php ' . base64_encode($playdata);
  $cmd .= '; cd ' . __DIR__;
  shell_exec($cmd);
  unset ($cmd, $playdata);
  
  if (empty($pause)) {
    clearstatcache();
    if (is_file(Flight::get('dncconf')['pausePath'])) {
      unlink(Flight::get('dncconf')['pausePath']);
    }
  }
  
  if (!empty($repeat)) {
    $data = [
      'time' => date('Y-m-d H:i:s', time() + (20)),
      'list' => $data
    ];
    Flight::updateschedule([$data]);
  }
  
  $stop = NULL;
  $state = 0;
  while (empty($stop)) {
    clearstatcache();
    if (is_file(Flight::get('dncconf')['queuePath'] . '/hold')) {
      for ($i = 0; $i < 4; $i++) {
        Flight::increase();
        sleep(2);
      }
      $stop = TRUE;
    }
    else {
      $state ++;
      if ($state == 1) {
        $cmd = '/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'amixer set Master 20%\'';
        shell_exec($cmd);
        unset ($cmd);
      }
      elseif ($state > 20) {
        $stop = TRUE;
      }
      sleep(1);
    }
  }
  unset ($stop, $state);
});

Flight::map('searchdata', function() {
  $cmd = '/usr/bin/ssh ' . Flight::get('dncconf')['playerUser'] . '@' . Flight::get('dncconf')['playerTarget'] . ' \'find ' . Flight::get('dncconf')['playerStorage'] . ' -name *.mp3\'';
  $result = shell_exec($cmd);
  unset ($cmd);
  $result = trim($result);
  $result = explode("\n", $result);
  if (empty($result)) {
    unset ($result);
    return;
  }
  $return = [];
  foreach ($result as $values) {
    $return[] = [
      'name' => basename($values),
      'path' => $values
    ];
  }
  unset ($result);
  return $return;
});
