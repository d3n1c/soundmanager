<?php

$user = trim(shell_exec('whoami'));

Flight::set('dncconf', [
  'playerTarget' => '127.0.0.1',
  'playerUser' => 'root',
  'playerStorage' => '/home/usman/Sounds',
  
  'schedulePath' => '/home/' . $user . '/soundScheduler',
  'pausePath' => '/tmp/pauseplayer',
  'queuePath' => '/tmp/audioplay',
  'playlistPath' => '/tmp/baseList',
  'reservePath' => '/tmp/recentplay',
  'keepVol' => '/tmp/volumeKeep',
  'defaultTZ' => 'Asia/Makassar'
]);

date_default_timezone_set(Flight::get('dncconf')['defaultTZ']);
