# soundManager

Micro services to manage sound player server. This tools act as server's daemon to handle sound player and control it's sound storage playlist and queue.
Besides, it also provides http services to serve front-end applications with an API interface.

**INSTALL**

clone or download its repository
install by composer
`composer install`

**USE**

1.  Edit `apps/config.php` to match the condition of the device you are using
2.  Run `apps/sound-daemon/daemon-play.php` in background
    `cd apps/sound-daemon; ./daemon-play.php &`
3.  Run `apps/sound-daemon/daemon-list.php` in background
    `cd apps/sound-daemon; ./daemon-list.php &`
4.  Run the playlist in the web server with the php built-in web server development
    `php -S 0.0.0.0:8000 -t apps/playlist`
5.  To read the API documentation, you can run the http service directed to the docs folder
    `php -S 0.0.0.0:8888 -t apps/docs`
6.  Akses the API documentation by URL above port 8888, e.g. http://127.0.0.1:8888

Feel free to improvised CLI using curl or create an html script for a simple front-end based on the API documentation above

enjoy :)